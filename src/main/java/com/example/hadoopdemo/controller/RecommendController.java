package com.example.hadoopdemo.controller;

import com.example.hadoopdemo.base.BaseResponse;
import com.example.hadoopdemo.executor.recommend.movie.RecommendClient;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 推荐算法案例
 *
 * @author Ruison
 * @date 2021/12/7
 */
@RestController
@RequestMapping("recommend")
@AllArgsConstructor
public class RecommendController {

    private final RecommendClient recommendClient;

    /**
     * 电影推荐
     *
     * @param jobName 任务名称
     * @param inputPath 文件路径，如果是文件的根路径，则默认该文件夹下所有的文件
     * @return 响应内容
     */
    @PostMapping("movie")
    public BaseResponse<?> recommend(@RequestParam("jobName") String jobName,
                                     @RequestParam("inputPath") String inputPath) {
        if (StringUtils.isEmpty(jobName) || StringUtils.isEmpty(inputPath)) {
            return BaseResponse.error("请求参数为空");
        }
        recommendClient.start(jobName, inputPath);
        return BaseResponse.ok();
    }

}
